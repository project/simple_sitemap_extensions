# Simple sitemap extensions

## Overview:

Extends simple sitemap to add support for sitemap index files and configuring
variants per index file.

With version 4.0 simple XML sitemap module also packs an index sitemap type,
but simple sitemap extension's index has one key advantage - you can specify
which sitemaps are shown in different indexes of sitemaps. This allows you to
create different sitemap indexes for any of your sub-sites that might share
content on the main site.

## Usage:

* install module

### Sitemap index

* configuration works the same way that configuring simple_sitemap works.
  * add sitemap type under `admin/config/search/simplesitemap/types`
    * note that Sitemap generator **Extended sitemap index generator** is only
      compatible with **Extended Sitemap variant URL generator** URL generator.
    * similarly for other URL generators. Simple sitemap extensions generators
      were not tested together with other URL generators.
  * add sitemap of that type under `/admin/config/search/simplesitemap`

* add one or more sitemaps of type "Extended Sitemap Index"
* add additional sitemaps you would need & save configuration
* in /admin/config/search/simplesitemap/settings
  set the default sitemap variant to the extended sitemap index
* add the variants which should be on a extended sitemap index on
  /admin/config/search/simplesitemap/sitemap-index
* regenerate sitemaps

### extended_entity (Extended entity) sitemap type
To get extra data for entities, as images.

For this a config file will be needed to be added.
`simple_sitemap_extensions.extended_entity.image_paths.yml`

Which should define the mapping to the images for entity.

Example for a node article with different fields that contain images

```
node:
  article:
    fields:
      field_hero:
        -
          bundles:
            - gallery
          fields:
            field_media:
              -
                bundles:
                  - image
                fields:
                  field_image: true
        -
          bundles:
            - image
          fields:
            field_image:
              -
                bundles:
                  - image
                fields:
                  field_image: true
      field_paragraphs:
        -
          bundles:
            - gallery
          fields:
            field_media:
              -
                bundles:
                  - image
                fields:
                  field_image: true
        -
          bundles:
            - image
          fields:
            field_image:
              -
                bundles:
                  - image
                fields:
                  field_image: true
      field_teaser_media:
        -
          bundles:
            - image
          fields:
            field_image: true
```

Then add an `extended_entity` sitemap variant:
* goto /admin/config/search/simplesitemap/variants
* add the new variant of type `extended_entity`
