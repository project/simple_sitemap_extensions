<?php

namespace Drupal\simple_sitemap_extensions;

use Drupal\simple_sitemap_extensions\Plugin\simple_sitemap\SitemapGenerator\DynamicSitemapGeneratorInterface;
use Drupal\simple_sitemap\Queue\QueueWorker;

/**
 * Sitemap queue worker with dynamic sitemap variant extension.
 *
 * @package Drupal\simple_sitemap_extension
 */
class DynamicSitemapQueueWorker extends QueueWorker {

  /**
   * {@inheritDoc}
   */
  protected function generateSitemapChunksFromResults(bool $complete = FALSE): void {
    $generator = $this->sitemapProcessedNow ? $this->sitemapProcessedNow->getType()->getSitemapGenerator() : NULL;
    if (!$generator instanceof DynamicSitemapGeneratorInterface) {
      parent::generateSitemapChunksFromResults($complete);
      return;
    }

    if (!empty($this->results)) {
      $processed_results = $this->results;
      $this->moduleHandler->alter('simple_sitemap_links', $processed_results, $this->sitemapProcessedNow);
      $this->processedResults = array_merge($this->processedResults, $processed_results);
      $this->results = [];
    }

    if (empty($this->processedResults)) {
      return;
    }

    $dynamic_chunks = $generator->getDynamicChunks($this->processedResults, $this->sitemapProcessedNow, $this->maxLinks);

    if (!empty($this->maxLinks)) {
      foreach ($dynamic_chunks as $dynamic_chunk) {
        if ($complete || count($dynamic_chunk) === $this->maxLinks) {
          $this->sitemapProcessedNow->addChunk($dynamic_chunk);
          $this->processedResults = array_diff_key($this->processedResults, $dynamic_chunk);
        }
      }
    }
    else {
      $this->sitemapProcessedNow->addChunk($this->processedResults);
      $this->processedResults = [];
    }
  }

}
