<?php

namespace Drupal\simple_sitemap_extensions;

use Drupal\simple_sitemap\Entity\SimpleSitemap;

/**
 * Helper functionality for extended sitemap index variants.
 */
trait SitemapIndexTrait {

  /**
   * Gets variants that are extended sitemap index variants.
   *
   * @return \Drupal\simple_sitemap\Entity\SimpleSitemap[]
   *   Extended sitemap indexes.
   */
  protected function getIndexVariants() {
    $variants = SimpleSitemap::loadMultiple();
    /** @var \Drupal\simple_sitemap\Entity\SimpleSitemapTypeStorage $type_storage */
    $type_storage = \Drupal::entityTypeManager()->getStorage('simple_sitemap_type');
    $types = $type_storage->loadMultiple();

    return array_filter($variants, function ($variant) use ($types) {
      /** @var \Drupal\simple_sitemap\Entity\SimpleSitemapType $type */
      $type = $types[$variant->getType()->id()];
      return $type->get('sitemap_generator') == 'sitemap_index';
    });
  }

  /**
   * Gets variants that are not extended sitemap index variants.
   *
   * @return \Drupal\simple_sitemap\Entity\SimpleSitemap[]
   *   Non-extended sitemap indexes.
   */
  protected function getNonIndexVariants() {
    $variants = SimpleSitemap::loadMultiple();
    /** @var \Drupal\simple_sitemap\Entity\SimpleSitemapTypeStorage $type_storage */
    $type_storage = \Drupal::entityTypeManager()->getStorage('simple_sitemap_type');
    $types = $type_storage->loadMultiple();

    return array_filter($variants, function ($variant) use ($types) {
      /** @var \Drupal\simple_sitemap\Entity\SimpleSitemapType $type */
      $type = $types[$variant->getType()->id()];
      return $type->get('sitemap_generator') != 'sitemap_index';
    });
  }

}
