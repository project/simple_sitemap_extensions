<?php

namespace Drupal\simple_sitemap_extensions\PathProcessor;

use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Component\HttpFoundation\Request;

/**
 * Processes the inbound and outbound sitemap paths.
 */
class ExtendedSitemapPathProcessor implements InboundPathProcessorInterface, OutboundPathProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    if (str_ends_with($path, '/sitemap.xml')) {
      // Remove an index prefix if existing in the form of
      // { index }/sub/{ variant }/{ chunk }/sitemap.xml.
      $args = explode('/', $path);
      if (!empty($args[2]) && $args[2] == 'sub') {
        unset($args[1], $args[2]);
        $path = implode('/', $args);
      }

      // Turn /{ variant }/{ chunk }/sitemap.xml back into
      // /sitemaps/{ variant }/{ chunk }/sitemap.xml.
      if (count($args) === 7 && $args[3] == 'sitemaps' && $args[2] = 'sub') {
        $path = '/sitemaps/' . $args[4] . '/' . $args[5] . '/' . $args[6];
      }
    }
    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], ?Request $request = NULL, ?BubbleableMetadata $bubbleable_metadata = NULL) {
    $args = explode('/', $path);
    // We map /sitemaps/{ variant }/{ chunk }/sitemap.xml
    // to /{ variant }/{ chunk }/sitemap.xml.
    // if (count($args) === 5 && $args[1] == 'sitemaps'
    // && $args[4] === 'sitemap.xml') {
    // $path = '/' . $args[2] . '/' . $args[3] . '/sitemap.xml';
    // already handled by simple_sitemaps
    // }.
    // Turns { path }/sitemap.xml into
    // { index }/sub/{ path }/sitemap.xml.
    if (!empty($options['sitemap_index'])) {
      $path = '/' . $options['sitemap_index'] . '/sub' . $path;
    }
    return $path;
  }

}
