<?php

namespace Drupal\simple_sitemap_extensions\Controller;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\State\StateInterface;
use Drupal\simple_sitemap\Controller\SimpleSitemapController;
use Drupal\simple_sitemap\Manager\Generator;
use Drupal\simple_sitemap\Manager\SitemapGetterTrait;
use Drupal\simple_sitemap_extensions\Plugin\simple_sitemap\SitemapGenerator\DynamicSitemapGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Extension of a SimplesitemapController.
 */
class DynamicSimplesitemapController extends SimpleSitemapController {

  use SitemapGetterTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The simple sitemap extensions logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructor.
   *
   * @param \Drupal\simple_sitemap\Manager\Generator $generator
   *   The simple_sitemap.generator service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\State\StateInterface $state
   *   State service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The simple sitemap extensions logger service.
   */
  public function __construct(Generator $generator, EntityTypeManagerInterface $entity_type_manager, StateInterface $state, LoggerChannelInterface $logger) {
    parent::__construct($generator);
    $this->entityTypeManager = $entity_type_manager;
    $this->state = $state;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SimpleSitemapController {
    return new static(
      $container->get('simple_sitemap.generator'),
      $container->get('entity_type.manager'),
      $container->get('state'),
      $container->get('logger.channel.simple_sitemap_extensions')
    );
  }

  /**
   * Returns a specific sitemap, its chunk, or its index.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param string|null $variant
   *   Optional name of sitemap variant.
   * @param string|null $chunk
   *   Optional the dynamic chunk id.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Returns an XML response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function getSitemap(Request $request, ?string $variant = NULL, ?string $chunk = NULL): Response {
    // Convert dynamic parameter into delta.
    if (isset($variant)) {
      /** @var \Drupal\simple_sitemap\Entity\SimpleSitemap $sitemap */
      $sitemap = $this->entityTypeManager->getStorage('simple_sitemap')->load($variant);
      if (!$sitemap) {
        $this->logger->error('Variant @variant not found!', ['@variant' => $variant]);
        throw new NotFoundHttpException();
      }
      $generator = $sitemap->getType()->getSitemapGenerator();
      $generator->setSitemap($sitemap);

      if ($generator instanceof DynamicSitemapGeneratorInterface && $chunk) {
        // Parameter was set by PathProcessorSitemapVariantIn.
        $delta = $generator->getCurrentDeltaFromMapping($chunk) ?: $chunk;
        $output = $this->generator->setSitemaps($variant)->getContent($delta);
        if (!$output) {
          throw new NotFoundHttpException();
        }

        return new Response($output, Response::HTTP_OK, [
          'Content-type' => 'application/xml; charset=utf-8',
          'X-Robots-Tag' => 'noindex, follow',
        ]);
      }
    }
    return parent::getSitemap($request, $variant);
  }

}
