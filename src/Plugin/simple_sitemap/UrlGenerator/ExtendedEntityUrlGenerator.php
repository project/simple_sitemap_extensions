<?php

namespace Drupal\simple_sitemap_extensions\Plugin\simple_sitemap\UrlGenerator;

use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
use Drupal\simple_sitemap\Entity\EntityHelper;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\simple_sitemap\Logger;
use Drupal\simple_sitemap\Manager\EntityManager;
use Drupal\simple_sitemap\Plugin\simple_sitemap\SimpleSitemapPluginBase;
use Drupal\simple_sitemap\Plugin\simple_sitemap\UrlGenerator\EntityUrlGenerator;
use Drupal\simple_sitemap\Plugin\simple_sitemap\UrlGenerator\UrlGeneratorManager;
use Drupal\simple_sitemap\Settings;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Extends the basis entity url generator.
 *
 * Adds support for deriving images from paragraphs contained in an entity and
 * the simple_sitemap_extensions_alter_dataset_entity_query() hook. Besides that
 * it matches the regular URL generator.
 *
 * @UrlGenerator(
 *   id = "extended_entity",
 *   label = @Translation("Extended entity URL generator"),
 *   description = @Translation("Generates URLs for entity bundles and bundle overrides."),
 * )
 */
class ExtendedEntityUrlGenerator extends EntityUrlGenerator {

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;


  /**
   * File url generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * {@inheritDoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Logger $logger,
    Settings $settings,
    LanguageManagerInterface $language_manager,
    EntityTypeManagerInterface $entity_type_manager,
    EntityHelper $entity_helper,
    EntityManager $entities_manager,
    UrlGeneratorManager $url_generator_manager,
    MemoryCacheInterface $memory_cache,
    ModuleHandlerInterface $module_handler,
    ConfigFactory $configFactory,
    FileUrlGeneratorInterface $fileUrlGenerator,
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $logger,
      $settings,
      $language_manager,
      $entity_type_manager,
      $entity_helper,
      $entities_manager,
      $url_generator_manager,
      $memory_cache
    );

    $this->moduleHandler = $module_handler;
    $this->configFactory = $configFactory;
    $this->fileUrlGenerator = $fileUrlGenerator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): SimpleSitemapPluginBase {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('simple_sitemap.logger'),
      $container->get('simple_sitemap.settings'),
      $container->get('language_manager'),
      $container->get('entity_type.manager'),
      $container->get('simple_sitemap.entity_helper'),
      $container->get('simple_sitemap.entity_manager'),
      $container->get('plugin.manager.simple_sitemap.url_generator'),
      $container->get('entity.memory_cache'),
      $container->get('module_handler'),
      $container->get('config.factory'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getDataSets(): array {
    $data_sets = [];
    $sitemap_entity_types = $this->entityHelper->getSupportedEntityTypes();

    $all_bundle_settings = $this->entitiesManager->setSitemaps($this->sitemap->id())->getAllBundleSettings();
    if (isset($all_bundle_settings[$this->sitemap->id()])) {
      foreach ($all_bundle_settings[$this->sitemap->id()] as $entity_type_name => $bundles) {
        if (!isset($sitemap_entity_types[$entity_type_name])) {
          continue;
        }

        if ($this->isOverwrittenForEntityType($entity_type_name)) {
          continue;
        }

        $entityTypeStorage = $this->entityTypeManager->getStorage($entity_type_name);
        $keys = $sitemap_entity_types[$entity_type_name]->getKeys();

        foreach ($bundles as $bundle_name => $bundle_settings) {
          if ($bundle_settings['index']) {
            $query = $entityTypeStorage->getQuery();

            if (!empty($keys['id'])) {
              $query->sort($keys['id']);
            }
            if (!empty($keys['bundle'])) {
              $query->condition($keys['bundle'], $bundle_name);
            }
            if (!empty($keys['published'])) {
              $query->condition($keys['published'], 1);
            }
            elseif (!empty($keys['status'])) {
              $query->condition($keys['status'], 1);
            }

            // Shift access check to EntityUrlGeneratorBase for language
            // specific access.
            // See https://www.drupal.org/project/simple_sitemap/issues/3102450.
            $query->accessCheck(FALSE);

            $this->moduleHandler->invokeAll('simple_sitemap_extensions_alter_dataset_entity_query', [
              $this,
              $query,
            ]);

            $data_set = [
              'entity_type' => $entity_type_name,
              'id' => [],
            ];
            foreach ($query->execute() as $entity_id) {
              $data_set['id'][] = $entity_id;
              if (count($data_set['id']) >= $this->entitiesPerDataset) {
                $data_sets[] = $data_set;
                $data_set['id'] = [];
              }
            }
            // Add the last data set if there are some IDs gathered.
            if (!empty($data_set['id'])) {
              $data_sets[] = $data_set;
            }
          }
        }
      }
    }

    return $data_sets;
  }

  /**
   * {@inheritDoc}
   */
  protected function getEntityImageData(ContentEntityInterface $entity): array {
    $image_paths = $this->configFactory->get('simple_sitemap_extensions.extended_entity.image_paths')->get();
    if (empty($image_paths[$entity->getEntityTypeId()][$entity->bundle()])) {
      return parent::getEntityImageData($entity);
    }

    $image_paths = $image_paths[$entity->getEntityTypeId()][$entity->bundle()];
    $image_data = $this->getImageDataFromImagePaths($entity, (array) $image_paths);

    return $image_data;
  }

  /**
   * Traverses the entity according to path configuration to fetch image data.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   A fieldable entity.
   * @param array $image_paths
   *   Configuration of image paths.
   *
   * @return array
   *   The data.
   */
  private function getImageDataFromImagePaths(FieldableEntityInterface $entity, array $image_paths) {
    $fields = !empty($image_paths['fields']) && is_array($image_paths['fields']) ? $image_paths['fields'] : [];
    $image_data = [];
    foreach ($fields as $field_name => $field_config) {
      if (!$entity->hasField($field_name)) {
        continue;
      }

      // The final field name will be set to TRUE, then fetch the data for it.
      if (is_bool($field_config) && $field_config) {
        $target_field = $entity->get($field_name);
        if ($target_field instanceof FileFieldItemList) {
          $image_data += $this->getImageDataFromFileField($target_field);
        }
      }
      elseif (is_array($field_config)) {
        foreach ($field_config as $item_config) {
          $required_bundles = !empty($item_config['bundles']) ? $item_config['bundles'] : [];
          $field = $entity->get($field_name);

          if ($field instanceof FileFieldItemList) {
            $image_data += $this->getImageDataFromFileField($field);
          }
          elseif ($field instanceof EntityReferenceFieldItemListInterface && !empty($item_config['fields'])) {
            foreach ($field as $field_item) {
              if (!$field_item->entity instanceof ContentEntityInterface) {
                continue;
              }
              if (!empty($required_bundles) && !in_array($field_item->entity->bundle(), $required_bundles)) {
                continue;
              }
              $image_data += $this->getEntityImageData($field_item->entity);
            }
          }
        }
      }
    }

    return $image_data;
  }

  /**
   * Gets the image data for an image field.
   *
   * @param \Drupal\file\Plugin\Field\FieldType\FileFieldItemList $field
   *   Image field.
   *
   * @return array
   *   The data.
   */
  private function getImageDataFromFileField(FileFieldItemList $field) {
    $image_data = [];
    foreach ($field->getValue() as $value) {
      if (NULL !== ($file = File::load($value['target_id']))) {
        $image_data[$value['target_id']] = [
          'path' => $this->replaceBaseUrlWithCustom(
            $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri())
          ),
          'alt' => $value['alt'],
          'title' => $value['title'],
        ];
      }
    }

    return $image_data;
  }

}
