<?php

namespace Drupal\simple_sitemap_extensions\Plugin\simple_sitemap\UrlGenerator;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\simple_sitemap\Logger;
use Drupal\simple_sitemap\Plugin\simple_sitemap\SimpleSitemapPluginBase;
use Drupal\simple_sitemap\Plugin\simple_sitemap\UrlGenerator\UrlGeneratorBase;
use Drupal\simple_sitemap\Settings;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\simple_sitemap_extensions\Plugin\simple_sitemap\SitemapGenerator\DynamicSitemapGeneratorInterface;
use Drupal\simple_sitemap_extensions\SitemapIndexTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Datetime\TimeInterface;

/**
 * Generates urls for sitemap variants.
 *
 * This is the URL generator used for the extended sitemap index.
 *
 * @UrlGenerator(
 *   id = "sitemap_variant",
 *   label = @Translation("Extended Sitemap variant URL generator"),
 *   description = @Translation("Generates URLs for sitemap variants."),
 * )
 */
class SitemapVariantUrlGenerator extends UrlGeneratorBase {

  use SitemapIndexTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    Logger $logger,
    Settings $settings,
    LanguageManagerInterface $language_manager,
    TimeInterface $time,
    ConfigFactoryInterface $config_factory,
    Connection $database,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger, $settings);
    $this->languageManager = $language_manager;
    $this->time = $time;
    $this->configFactory = $config_factory;
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): SimpleSitemapPluginBase {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('simple_sitemap.logger'),
      $container->get('simple_sitemap.settings'),
      $container->get('language_manager'),
      $container->get('datetime.time'),
      $container->get('config.factory'),
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDataSets(): array {
    $data_sets = [];
    $variants = $this->getNonIndexVariants();

    // @todo Since sitemap is now entity - this is not rly needed!
    $config = $this->configFactory->get('simple_sitemap_extensions.sitemap_index.settings');
    $index_config = (array) $config->get($this->sitemap->id());
    $enabled_variants = $index_config['variants'] ?? [];

    foreach ($variants as $variant_key => $variant_definition) {
      if (!in_array($variant_key, $enabled_variants)) {
        continue;
      }
      $data_sets[] = ['variant' => $variant_key];
    }
    return $data_sets;
  }

  /**
   * Gets the custom base url.
   *
   * @return string
   *   The URL.
   */
  protected function getCustomBaseUrl() {
    $customBaseUrl = $this->settings->get('base_url');
    return !empty($customBaseUrl) ? $customBaseUrl : $GLOBALS['base_url'];
  }

  /**
   * Get the number of pages for a given variant.
   *
   * @param string $sitemapVariant
   *   The sitemap variant.
   *
   * @return array
   *   Pages for the variant indexed by delta.
   */
  private function getNumberOfVariantPages($sitemapVariant) {
    $pages = $this->database->select('simple_sitemap', 's')
      ->fields('s', ['delta', 'sitemap_created', 'type'])
      ->condition('s.type', $sitemapVariant)
      ->condition('s.status', 1)
      ->orderBy('delta', 'ASC')
      ->execute()
      ->fetchAllAssoc('delta');

    return (array) $pages;
  }

  /**
   * {@inheritdoc}
   */
  public function generate($data_set): array {
    $path_data = $this->processDataSet($data_set);
    return FALSE !== $path_data ? $path_data : [];
  }

  /**
   * {@inheritdoc}
   */
  protected function processDataSet($data_set): array {
    $settings = [
      'absolute' => TRUE,
      'base_url' => $this->getCustomBaseUrl(),
      'language' => $this->languageManager->getDefaultLanguage(),
      // Provide additional context for the URL outbound processing.
      'sitemap_index' => $this->sitemap->id(),
    ];

    $pages = $this->getNumberOfVariantPages($data_set['variant']);
    /** @var \Drupal\simple_sitemap\Entity\SimpleSitemap $sitemap */
    $sitemap = $this->entityTypeManager->getStorage('simple_sitemap')->load($data_set['variant']);
    $generator = $sitemap->getType()->getSitemapGenerator();
    $generator->setSitemap($sitemap);

    if (count($pages) > 1 || $generator instanceof DynamicSitemapGeneratorInterface) {
      $urls = [];
      foreach ($pages as $delta => $page) {
        // Skip index.
        if ($delta == 0) {
          continue;
        }

        if ($generator instanceof DynamicSitemapGeneratorInterface) {
          $url = $generator->getSitemapUrl($delta, $settings);
        }
        else {
          // @todo This duplicates $generator->getSitemapUrl() - needs fix
          // to re-use it while keeping our settings.
          $parameters = [
            'page' => $delta,
            'variant' => $data_set['variant'],
          ];
          $url = Url::fromRoute('simple_sitemap.sitemap_variant', $parameters, $settings);
        }

        $url = [
          'url' => $url instanceof Url ? $url->toString() : (string) $url,
          'lastmod' => date('c', $this->time->getRequestTime()),
          'langcode' => $this->languageManager->getDefaultLanguage()->getId(),
        ];
        $urls[] = $url;
      }

      return $urls;
    }
    else {
      $url = Url::fromRoute('simple_sitemap.sitemap_variant', ['variant' => $data_set['variant']], $settings);
      return [
        [
          'url' => $url instanceof Url ? $url->toString() : (string) $url,
          'lastmod' => date('c', $this->time->getRequestTime()),
          'langcode' => $this->languageManager->getDefaultLanguage()->getId(),
        ],
      ];
    }
  }

}
