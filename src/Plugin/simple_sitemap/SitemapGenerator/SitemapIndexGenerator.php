<?php

namespace Drupal\simple_sitemap_extensions\Plugin\simple_sitemap\SitemapGenerator;

use Drupal\simple_sitemap\Plugin\simple_sitemap\SitemapGenerator\DefaultSitemapGenerator;

/**
 * Generator for extended sitemap index of variants.
 *
 * @package Drupal\simple_sitemap\Plugin\simple_sitemap\SitemapGenerator
 *
 * @SitemapGenerator(
 *   id = "sitemap_index",
 *   label = @Translation("Extended sitemap index generator"),
 *   description = @Translation("Generates an extended sitemap index containing links to all sitemap variants."),
 * )
 */
class SitemapIndexGenerator extends DefaultSitemapGenerator {

  /**
   * The attributes.
   *
   * @var array
   */
  protected static $attributes = [
    'xmlns' => self::XMLNS,
  ];

  /**
   * {@inheritdoc}
   */
  public function getChunkContent(array $links): string {
    $this->writer->openMemory();
    $this->writer->setIndent(TRUE);
    $this->writer->startSitemapDocument();

    // Add the XML stylesheet to document if enabled.
    if ($this->settings->get('xsl')) {
      $this->writer->writeXsl($this->getPluginId());
    }

    $this->writer->writeGeneratedBy();
    $this->writer->startElement('sitemapindex');

    // Add attributes to document.
    $this->addSitemapAttributes();

    $sitemap_variant = $this->sitemap->id();
    $this->moduleHandler->alter('simple_sitemap_links', $links, $sitemap_variant);
    foreach ($links as $link) {
      $this->writer->startElement('sitemap');
      $this->writer->writeElement('loc', is_string($link['url']) ? $link['url'] : $link['url']->toString());

      // Add lastmod if any.
      if (isset($link['lastmod'])) {
        $this->writer->writeElement('lastmod', $link['lastmod']);
      }

      // End element: sitemap.
      $this->writer->endElement();
    }

    // End element: sitemapindex.
    $this->writer->endElement();
    $this->writer->endDocument();

    $result = $this->writer->outputMemory();
    return $result;
  }

}
