<?php

namespace Drupal\simple_sitemap_extensions\Plugin\simple_sitemap\SitemapGenerator;

use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\simple_sitemap\Entity\SimpleSitemapInterface;
use Drupal\simple_sitemap\Plugin\simple_sitemap\SimpleSitemapPluginBase;
use Drupal\simple_sitemap\Plugin\simple_sitemap\SitemapGenerator\DefaultSitemapGenerator;
use Drupal\simple_sitemap\Plugin\simple_sitemap\SitemapGenerator\SitemapWriter;
use Drupal\simple_sitemap\Settings;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Generates a dynamic sitemap containing links to dynamic sitemap chunks.
 *
 * Dynamic generator need to extend this class.
 */
abstract class AbstractDynamicSitemapGenerator extends DefaultSitemapGenerator implements DynamicSitemapGeneratorInterface {

  const DYNAMIC_GENERATOR_ID = 'dynamic_sitemap_generator';

  const DYNAMIC_GENERATOR_PARAMETER_NAME = 'dynamic-parameter';

  const FIRST_CHUNK_DELTA = 1;

  /**
   * Drupal state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Dynamic delta mapping.
   *
   * @var array|null
   */
  protected $dynamicDeltaMapping = NULL;

  /**
   * Which sitemap variant current delta mapping applies to.
   *
   * @var string
   */
  protected $currentVariantMapping = '';

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ModuleHandlerInterface $module_handler,
    SitemapWriter $sitemap_writer,
    Settings $settings,
    StateInterface $state,
    ModuleExtensionList $module_list,
    LanguageManagerInterface $language_manager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $module_handler, $sitemap_writer, $settings, $module_list);
    $this->state = $state;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): SimpleSitemapPluginBase {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('simple_sitemap.sitemap_writer'),
      $container->get('simple_sitemap.settings'),
      $container->get('state'),
      $container->get('extension.list.module'),
      $container->get('language_manager')
    );
  }

  /**
   * Get sitemap url.
   *
   * @param mixed $delta
   *   Which month to fetch.
   * @param array|null $settings
   *   (optional) If set, custom sitemap URL settings to apply.
   *
   * @return \Drupal\Core\Url
   *   Url of a sitemap.
   */
  public function getSitemapUrl($delta = NULL, ?array $settings = NULL) {
    // $this->sitemap->toUrl('canonical', ['delta' => $delta])->toString();
    if ($this->sitemap->isDefault() || !isset($delta)) {
      // @todo selecting dynamic variant as default might cause problems
      // with the url - needs testing.
      return $this->sitemap->toUrl('canonical', ['delta' => 1]);
    }
    else {
      $chunk = $this->getCurrentChunkParameterFromMapping($delta);
      return Url::fromRoute(
        'simple_sitemap_extensions.sitemap_variant_page',
        [
          'chunk' => $chunk ?: $delta,
          'variant' => $this->sitemap->id(),
        ],
        $settings ?? $this->getDefaultSitemapUrlSettings()
      );
    }
  }

  /**
   * Get base url options for a sitemap.
   *
   * @return array
   *   Options array for Drupal\Core\Url.
   */
  protected function getDefaultSitemapUrlSettings() {
    return [
      'absolute' => TRUE,
      'base_url' => $this->getCustomBaseUrl(),
      'language' => $this->languageManager->getLanguage(LanguageInterface::LANGCODE_NOT_APPLICABLE),
    ];
  }

  /**
   * Gets the custom base url.
   *
   * @return string
   *   The URL.
   */
  protected function getCustomBaseUrl() {
    $customBaseUrl = $this->settings->get('base_url');
    return !empty($customBaseUrl) ? $customBaseUrl : $GLOBALS['base_url'];
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentChunkParameterFromMapping(int $delta) {
    if (empty($this->dynamicDeltaMapping) || $this->sitemap->id() != $this->currentVariantMapping) {
      $this->dynamicDeltaMapping = $this->state->get(static::DYNAMIC_GENERATOR_ID . '_' . $this->sitemap->id(), FALSE);
      $this->currentVariantMapping = $this->sitemap->id();
    }
    return $this->dynamicDeltaMapping[$delta - static::FIRST_CHUNK_DELTA] ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentDeltaFromMapping($chunk = NULL) {
    if (!$chunk) {
      return 0;
    }
    if (empty($this->dynamicDeltaMapping) || $this->sitemap->id() != $this->currentVariantMapping) {
      $this->dynamicDeltaMapping = $this->state->get(static::DYNAMIC_GENERATOR_ID . '_' . $this->sitemap->id(), FALSE);
      $this->currentVariantMapping = $this->sitemap->id();
    }
    return array_search($chunk, $this->dynamicDeltaMapping) + static::FIRST_CHUNK_DELTA;
  }

  /**
   * {@inheritdoc}
   */
  public function getDynamicChunks($results, SimpleSitemapInterface $variant, $max_links = NULL) {
    // Create dynamic chunks.
    $dynamic_chunks = [];
    foreach ($results as $key => $link) {
      // Url generator must also include dynamic parameter to create chunks
      // from it.
      $dynamic_chunks[$link['meta'][static::DYNAMIC_GENERATOR_PARAMETER_NAME]][$key] = $link;
    }
    $dynamic_chunks_max_links = [];
    if (!empty($max_links)) {
      foreach ($dynamic_chunks as $dynamic_parameter => $dynamic_chunk) {
        $max_links_chunks = array_chunk($dynamic_chunk, $max_links, TRUE);
        $counter = 1;
        foreach ($max_links_chunks as $max_links_chunk) {
          $dynamic_chunks_max_links[$dynamic_parameter . '-' . (string) ($counter)] = $max_links_chunk;
          $counter++;
        }
      }
    }
    else {
      foreach ($dynamic_chunks as $dynamic_parameter => $dynamic_chunk) {
        $dynamic_chunks_max_links[$dynamic_parameter . '-1'] = $dynamic_chunk;
      }
    }
    // Keep a mapping of the page delta counter to dynamic chunks.
    $this->state->set(static::DYNAMIC_GENERATOR_ID . '_' . $variant->id(), array_keys($dynamic_chunks_max_links));
    return $dynamic_chunks_max_links;
  }

  /**
   * {@inheritdoc}
   */
  public function getIndexContent(): string {
    if ($this->sitemap->isDefault()) {
      return parent::getIndexContent();
    }

    $this->writer->openMemory();
    $this->writer->setIndent(TRUE);
    $this->writer->startSitemapDocument();

    // Add the XML stylesheet to document if enabled.
    if ($this->settings->get('xsl')) {
      $this->writer->writeXsl($this->getPluginId());
    }

    $this->writer->writeGeneratedBy();
    $this->writer->startElement('sitemapindex');

    // Add attributes to document.
    $attributes = self::$indexAttributes;
    $this->moduleHandler->alter('simple_sitemap_index_attributes', $attributes, $this->sitemap);
    foreach ($attributes as $name => $value) {
      $this->writer->writeAttribute($name, $value);
    }

    // Add sitemap chunk locations to document.
    for ($delta = 1; $delta <= $this->sitemap->fromUnpublished()->getChunkCount(); $delta++) {
      $this->writer->startElement('sitemap');
      $this->writer->writeElement('loc', $this->getSitemapUrl($delta)->toString());
      $this->writer->writeElement('lastmod', date('c', $this->sitemap->fromUnpublished()->getCreated()));
      $this->writer->endElement();
    }

    $this->writer->endElement();
    $this->writer->endDocument();

    return $this->writer->outputMemory();
  }

}
